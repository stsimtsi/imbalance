document.getElementById("hamburger").addEventListener("click", function(){
  this.classList.toggle("active");
  document.querySelector(".mobile-menu").classList.toggle("active");
});

// // When the user scrolls the page, execute myFunction
// window.onscroll = function() {myFunction()};

// // Get the header
// var header = document.getElementByClassName("header");

// // Get the offset position of the navbar
// var sticky = header.offsetTop;

// // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
// function myFunction() {
//   if (window.pageYOffset > sticky) {
//     header.classList.add("header-sticky");
//   } else {
//     header.classList.remove("header-sticky");
//   }
// }
if (window.innerWidth > 768) {
  window.onscroll = function() {scrollFunction()};
}
var header = document.getElementById("header");
function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    header.style.backgroundColor = '#000000';
  } else {
    header.style.backgroundColor = 'rgba(0, 0, 0, 0)';
  }
}

//gallery

(function($){

  $(document).ready(function() {
  let images = document.querySelectorAll('.gallery-image');
  let modal = document.querySelector('.modal');

  if (images.length) {
    let modal = document.querySelector('.modal');

    images.forEach(function(image) {
      image.addEventListener('click', function() {
        let src = this.getAttribute('data-default-img');
        modal.querySelector('img').setAttribute('src', src);
        modal.classList.add('active');
      });
    })
  }

  if (modal != null) {
    modal.addEventListener('click', function(e) {
      if (e.target === this) {
        this.classList.remove('active')
      }
    });
  }


  });

})(jQuery);

//404
var canvas = document.getElementById('canvas');
  if (canvas) {
    context = canvas.getContext('2d');
    height = canvas.height = 256;
    width = canvas.width = height;
    bcontext = 'getCSSCanvasContext' in document ? document.getCSSCanvasContext('2d', 'noise', width, height) : context;
noise();
  }

function noise() {
    requestAnimationFrame(noise);
    var idata = context.getImageData(0, 0, width, height);
    for (var i = 0; i < idata.data.length; i += 4) {
        idata.data[i] = idata.data[i + 1] = idata.data[i + 2] = Math.floor(Math.random() * 255);
        idata.data[i + 3] = 255;
    }
    bcontext.putImageData(idata, 0, 0);
}

