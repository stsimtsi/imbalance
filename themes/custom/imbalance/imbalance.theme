<?php

use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Implements theme_preprocess().
 */
function imbalance_preprocess(&$variables, $hook) {
  try {
    $variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
  }
  catch (Exception $e) {
    $variables['is_front'] = FALSE;
  }
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] = $language;
  $variables['c_path'] = \Drupal::service('path.current')->getPath();
  // Ensure the cache varies correctly (new in Drupal 8.3).
  $variables['#cache']['contexts'][] = 'url.path.is_front';
}

/**
 * Implements hook_preprocess_html().
 *
 */
function imbalance_preprocess_html(&$variables) {
  $variables['node'] = \Drupal::routeMatch()->getParameter('node');
  $variables['term'] = \Drupal::routeMatch()->getParameter('taxonomy_term');
  $variables['route_name'] = \Drupal::routeMatch()->getRouteName();
  // to remove extra div. Keep it?
  $variables["page"]["content"]["messages"]["#include_fallback"] = false;
}


/**
 * Implements theme_preprocess_page().
 */
function imbalance_preprocess_page(&$variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();

  if (in_array($route_name, ['system.401', 'system.403', 'system.404'])) {
    $variables['#attached']['library'][] = 'imbalance/error';
  }

  switch ($route_name) {
    case 'entity.node.preview':
    case 'spages.contact':
      $variables['page']['sidebar'] = false;
      break;
  }

  /** @var \Drupal\node\NodeInterface $node */
  if (isset($variables['node'])) {
    $node = $variables['node'];
    switch ($node->getType()) {
      case 'article':
      case 'page':
        $variables['page']['sidebar'] = false;
        break;
    }
  }

  if ($term = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
    switch ($term->bundle()) {
      case 'author':
        $variables['page']['title'] = $term->getName();
        break;
      case 'category':
        $variables['#attached']['library'][] = 'imbalance/category';
        break;
    }
  }

}

/**
 * Implements theme_preprocess_node().
 */
function imbalance_preprocess_node(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];

  // Provide a distinct $teaser boolean.
  $variables['teaser'] = $variables['view_mode'] == 'teaser';
  $variables['node'] = $variables['elements']['#node'];

  if ($variables['view_mode'] == 'full') {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $variables['node'];
    switch ($node->getType()) {
      case 'project':
        $variables['#attached']['library'][] = 'imbalance/project';
        break;
      case 'page':
        $variables['#attached']['library'][] = 'imbalance/page';
        break;
    }

    if ($node->body->value) {
      $theme = \Drupal::theme()->getActiveTheme();
      $body_view = $node->body->view();
      $body = render($body_view);
      $variables['processed_body'] = $body;
    }
  }

}

function imbalance_preprocess_page_title(&$variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  $variables['route_name'] = $route_name;
  $pager = \Drupal::request()->query->get('page');
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $variables['title'] = false;
  }
  elseif ($term = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
    if (in_array($term->bundle(), ['author'])) {
      $variables['title'] = false;
    }
    $variables['extra_class'] = ' title-area--sidebar';
    $variables['term'] = $term;
  }
  elseif (in_array($route_name, [
    'entity.node.preview',
    'spages.home',
  ])) {
    $variables['title'] = false;
  }
}

function imbalance_preprocess_status_messages(&$variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  if (!$variables['logged_in'] || $route_name == 'spages.contact') {
    $variables['message_list'] = [];
  }
}

/**
 * Implements theme_preprocess_views_view().
 */
function imbalance_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  if (isset($view->params['display'])) {
    switch ($view->params['display']) {
      case 'default_teaser':
        $variables['#attached']['library'][] = 'imbalance/default_teaser';
        break;
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for node templates.
 */
function imbalance_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {
  $view = $variables['view'];
  if (isset($view->params['display'])) {
    switch ($view->params['display']) {
      case 'default_teaser':
        $suggestions[] = 'views_view__default_teaser';
        break;
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function imbalance_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  switch ($route_name) {
    case 'system.401':
      // Unauthorized Access.
      $error = 401;
      $suggestions[] = 'page__error';
      break;
    case 'system.403':
      // Access Denied.
      $error = 403;
      $suggestions[] = 'page__error';
      break;
    case 'system.404':
      // Page Not Found.
      $error = 404;
      $suggestions[] = 'page__error';
      break;
  }
}

function imbalance_page_attachments_alter(array &$page) {
  // Replace http with https in meta tags
  foreach ($page['#attached']['html_head'] as $key => $value){
    if (isset($value[0]['#attributes']['href'])) {
      $page['#attached']['html_head'][$key][0]['#attributes']['href'] = str_replace('http:','https:',$value[0]['#attributes']['href']);
    }
    if (isset($value[0]['#attributes']['content'])) {
      $page['#attached']['html_head'][$key][0]['#attributes']['content'] = str_replace('http:','https:',$value[0]['#attributes']['content']);
    }
  }
}

/**
 * Implements template_preprocess_pager().
 */
function imbalance_preprocess_pager(&$variables) {
  $element = $variables['pager']['#element'];
  $parameters = $variables['pager']['#parameters'];
  $quantity = empty($variables['pager']['#quantity']) ? 0 : $variables['pager']['#quantity'];
  $route_name = $variables['pager']['#route_name'];
  $route_parameters = isset($variables['pager']['#route_parameters']) ? $variables['pager']['#route_parameters'] : [];

  /* @var $pager_manager \Drupal\Core\Pager\PagerManagerInterface */
  $pager_manager = \Drupal::service('pager.manager');
  $pager = $pager_manager
    ->getPager($element);

  // Nothing to do if there is no pager.
  if (!isset($pager)) {
    return;
  }
  $pager_max = $pager
    ->getTotalPages();

  // Nothing to do if there is only one page.
  if ($pager_max <= 1) {
    return;
  }
  $tags = $variables['pager']['#tags'];

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  $current_page = $pager
    ->getCurrentPage();

  // The current pager is the page we are currently paged to.
  $pager_current = $current_page + 1;

  // The first pager is the first page listed by this pager piece (re quantity).
  $pager_first = $pager_current - $pager_middle + 1;

  // The last is the last page listed by this pager piece (re quantity).
  $pager_last = $pager_current + $quantity - $pager_middle;

  // End of marker calculations.
  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {

    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {

    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  // End of generation loop preparation.
  // Create the "first" and "previous" links if we are not on the first page.
  if ($current_page > 0) {
    $items['first'] = [];
    $items['first']['attributes'] = new Attribute();
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, 0),
    ];
    $items['first']['href'] = Url::fromRoute($route_name, $route_parameters, $options)
      ->toString();
    if (isset($tags[0])) {
      $items['first']['text'] = $tags[0];
    }
    $items['previous'] = [];
    $items['previous']['attributes'] = new Attribute();
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, $current_page - 1),
    ];
    $items['previous']['href'] = Url::fromRoute($route_name, $route_parameters, $options)
      ->toString();
    if (isset($tags[1])) {
      $items['previous']['text'] = $tags[1];
    }
  }
  if ($i != $pager_max) {

    // Add an ellipsis if there are further previous pages.
    if ($i > 1) {
      $variables['ellipses']['previous'] = TRUE;
    }

    // Now generate the actual pager piece.
    for (; $i <= $pager_last && $i <= $pager_max; $i++) {
      $options = [
        'query' => $pager_manager
          ->getUpdatedParameters($parameters, $element, $i - 1),
      ];
      $items['pages'][$i]['href'] = Url::fromRoute($route_name, $route_parameters, $options)
        ->toString();
      $items['pages'][$i]['attributes'] = new Attribute();
      if ($i == $pager_current) {
        $variables['current'] = $i;
      }
    }

    // Add an ellipsis if there are further next pages.
    if ($i < $pager_max + 1) {
      $variables['ellipses']['next'] = TRUE;
    }
  }

  // Create the "next" and "last" links if we are not on the last page.
  if ($current_page < $pager_max - 1) {
    $items['next'] = [];
    $items['next']['attributes'] = new Attribute();
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, $current_page + 1),
    ];
    $items['next']['href'] = Url::fromRoute($route_name, $route_parameters, $options)
      ->toString();
    if (isset($tags[3])) {
      $items['next']['text'] = $tags[3];
    }
    $items['last'] = [];
    $items['last']['attributes'] = new Attribute();
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, $pager_max - 1),
    ];
    $items['last']['href'] = Url::fromRoute($route_name, $route_parameters, $options)
      ->toString();
    if (isset($tags[4])) {
      $items['last']['text'] = $tags[4];
    }
  }
  $variables['items'] = $items;
  $variables['heading_id'] = Html::getUniqueId('pagination-heading');

  // The rendered link needs to play well with any other query parameter used
  // on the page, like exposed filters, so for the cacheability all query
  // parameters matter.
  $variables['#cache']['contexts'][] = 'url.query_args';

  // Use the base path if on page 2 otherwise `page={{current_page-1}}`.
  $prev_href = isset($items['previous']) && $items['previous']['href']? $items['previous']['href'] : NULL;
  $next_href = isset($items['next']) && $items['next']['href'] ? $items['next']['href'] : NULL;

  // Add The prev rel link.
  if ($prev_href) {
    $variables['#attached']['html_head_link'][] = [
      [
        'rel' => 'prev',
        'href' => $prev_href,
      ],
      TRUE,
    ];
  }

  // Add the next rel link.
  if ($next_href) {
    $variables['#attached']['html_head_link'][] = [
      [
        'rel' => 'next',
        'href' => $next_href,
      ],
      TRUE,
    ];
  }

}

function imbalance_preprocess_links__language_block(&$variables) {
  $variables['links']['el']['link']['#title'] = 'EL';
  $variables['links']['en']['link']['#title'] = 'EN';

  $node = \Drupal::routeMatch()->getParameter('node');
  if(!$node) {
    return;
  }
  foreach (array_keys($variables['links']) as $lang) {
    if (!$node->hasTranslation($lang)) {
      unset($variables['links'][$lang]);
    }
  }
}

/**
 * Implements hook_preprocess_views_mini_pager().
 */
function imbalance_preprocess_views_mini_pager(&$variables) {

  /* @var $pager_manager \Drupal\Core\Pager\PagerManagerInterface */
  $pager_manager = \Drupal::service('pager.manager');
  $tags =& $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $pager = $pager_manager
    ->getPager($element);
  if (!$pager) {
    return;
  }
  $current = $pager
    ->getCurrentPage();
  $total = $pager
    ->getTotalPages();

  // Current is the page we are currently paged to.
  $variables['items']['current'] = $current + 1;
  if ($total > 1 && $current > 0) {
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, $current - 1),
    ];
    $variables['items']['previous']['href'] = Url::fromRoute('<current>', [], $options)
      ->toString();
    if (isset($tags[1])) {
      $variables['items']['previous']['text'] = $tags[1];
    }
    $variables['items']['previous']['attributes'] = new Attribute();
  }
  if ($current < $total - 1) {
    $options = [
      'query' => $pager_manager
        ->getUpdatedParameters($parameters, $element, $current + 1),
    ];
    $variables['items']['next']['href'] = Url::fromRoute('<current>', [], $options)
      ->toString();
    if (isset($tags[3])) {
      $variables['items']['next']['text'] = $tags[3];
    }
    $variables['items']['next']['attributes'] = new Attribute();
  }

  // This is based on the entire current query string. We need to ensure
  // cacheability is affected accordingly.
  $variables['#cache']['contexts'][] = 'url.query_args';
  $variables['heading_id'] = Html::getUniqueId('pagination-heading');

//  $prev_href = ($pager_total[$element] > 1 && $pager_page_array[$element] > 0) ? $variables['items']['previous']['href'] : NULL;
//  $next_href = $pager_page_array[$element] < $pager_total[$element] - 1 ? $variables['items']['next']['href'] : NULL;
//
//  // Add The prev rel link.
//  if ($prev_href) {
//    $variables['#attached']['html_head_link'][] = [
//      [
//        'rel' => 'prev',
//        'href' => $prev_href,
//      ],
//      TRUE,
//    ];
//  }
//
//  // Add the next rel link.
//  if ($next_href) {
//    $variables['#attached']['html_head_link'][] = [
//      [
//        'rel' => 'next',
//        'href' => $next_href,
//      ],
//      TRUE,
//    ];
//  }


}
