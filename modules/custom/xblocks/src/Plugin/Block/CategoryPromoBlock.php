<?php

namespace Drupal\xblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'Category Promo' Block.
 *
 * @Block(
 *   id = "xblocks_category_promo",
 *   admin_label = @Translation("Category Promo"),
 *   category = @Translation("xblock"),
 * )
 */
class CategoryPromoBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $data = [
      'term' => $term,
    ];

    return [
      '#theme' => 'xblocks_category_promo',
      '#data' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $show = false;
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $page = \Drupal::request()->query->get('page');
    if ($term && $term->bundle() == 'category' && !$page && $term->parent->target_id == 0) {
      $show = true;
    }
    return AccessResult::allowedIf($show);
  }

}
