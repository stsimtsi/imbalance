<?php

namespace Drupal\xblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'Home promo' Block.
 *
 * @Block(
 *   id = "xblocks_home_primary",
 *   admin_label = @Translation("Home primary"),
 *   category = @Translation("xblock"),
 * )
 */
class HomePrimaryBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nids = xget_queue_ids('homepage');

    $data = [
      'nids' => implode('+', $nids),
    ];

    return [
      '#theme' => 'xblocks_home_primary',
      '#data' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $is_front = \Drupal::service('path.matcher')->isFrontPage();
    return AccessResult::allowedIf($is_front);
  }

}
