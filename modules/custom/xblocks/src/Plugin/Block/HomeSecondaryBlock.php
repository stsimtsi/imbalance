<?php

namespace Drupal\xblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\entityqueue\Entity\EntitySubqueue;

/**
 * Provides a 'Home bottom' Block.
 *
 * @Block(
 *   id = "xblocks_home_secondary",
 *   admin_label = @Translation("Home secondary"),
 *   category = @Translation("xblock"),
 * )
 */
class HomeSecondaryBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nids = xget_queue_ids('homepage');

    $data = [
      'nids' => implode('+', $nids),
    ];

    return [
      '#theme' => 'xblocks_home_secondary',
      '#data' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $is_front = \Drupal::service('path.matcher')->isFrontPage();
    return AccessResult::allowedIf($is_front);
  }

}
