<?php

namespace Drupal\xblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'Latest' Block.
 *
 * @Block(
 *   id = "xblocks_latest",
 *   admin_label = @Translation("Latest"),
 *   category = @Translation("xblock"),
 * )
 */
class LatestBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'xblocks_latest',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $node = \Drupal::routeMatch()->getParameter('node');
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');

    $show = ($node && in_array($node->bundle(), ['article','tribute'])) || ($term && $term->bundle() !== 'author');
    return AccessResult::allowedIf($show);
  }

}
