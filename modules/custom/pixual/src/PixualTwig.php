<?php

namespace Drupal\pixual;

/**
 * Twig extension with some useful functions and filters.
 *
 * Dependency injection is not used for performance reason.
 */
class PixualTwig extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      // new \Twig_SimpleFunction('pager_find_page', 'pager_find_page'),
      // new \Twig_SimpleFunction('xembed_view', 'xembed_view'),
      new \Twig_SimpleFunction('xget_vocabulary_tree', 'xget_vocabulary_tree'),
      new \Twig_SimpleFunction('xget_vocabulary_tree_loaded', 'xget_vocabulary_tree_loaded'),
      new \Twig_SimpleFunction('xstrtoupper', 'xstrtoupper'),
      // new \Twig_SimpleFunction('xget_tid_name', 'xget_tid_name'),
      // new \Twig_SimpleFunction('strtolower', 'mb_strtolower'),
      // new \Twig_SimpleFunction('xlimit_chars', 'xlimit_chars'),
      // new \Twig_SimpleFunction('pixual_is_taxonomy_page', 'pixual_is_taxonomy_page'),
      new \Twig_SimpleFunction('xget_queue', 'xget_queue'),
      new \Twig_SimpleFunction('xget_queue_ids', 'xget_queue_ids'),
      new \Twig_SimpleFunction('xget_term', 'xget_term'),
      new \Twig_SimpleFunction('xget_terms', 'xget_terms'),
      // new \Twig_SimpleFunction('xtargeting', 'xtargeting'),
      new \Twig_SimpleFunction('pixual_arg', 'pixual_arg'),
      // new \Twig_SimpleFunction('pixual_is_home_page', 'pixual_is_home_page'),
      // new \Twig_SimpleFunction('pixual_get_current_route', 'pixual_get_current_route'),
      // new \Twig_SimpleFunction('pixual_get_current_node', 'pixual_get_current_node'),
      new \Twig_SimpleFunction('pixual_drupalEntity', 'pixual_drupalEntity'),
      // new \Twig_SimpleFunction('xformat_date', 'xformat_date'),
      // new \Twig_SimpleFunction('pixual_get_list_value', 'pixual_get_list_value'),
      // new \Twig_SimpleFunction('pixual_reading_time', 'pixual_reading_time'),
      // new \Twig_SimpleFunction('xget_related_articles_of_node', 'xget_related_articles_of_node'),
      // new \Twig_SimpleFunction('format_size', 'format_size'),
      // new \Twig_SimpleFunction('xmisc_get_weather', 'xmisc_get_weather'),
      // new \Twig_SimpleFunction('xdropcap', [$this, 'xdropcap']),
      // new \Twig_SimpleFunction('xpager_offset', [$this, 'xpager_offset']),
      // new \Twig_SimpleFunction('xget_promo_articles_for_term', [$this, 'xget_promo_articles_for_term']),
      // new \Twig_SimpleFunction('xget_author_initials', [$this, 'xget_author_initials']),
      // new \Twig_SimpleFunction('xget_file_size', [$this, 'xget_file_size']),
      // new \Twig_SimpleFunction('xmarket_get_data', 'xmarket_get_data'),
      // new \Twig_SimpleFunction('xremove_nbsp', 'xremove_nbsp'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  // public function getName() {
  //   return 'pixual_twig';
  // }

  // public function xdropcap($text = '') {
  //   $output = '';
  //   $text = strip_tags($text);
  //   $text = trim($text);
  //   if (mb_strlen($text) > 0) {
  //     $output = mb_substr($text, 0, 1);
  //   }
  //   return $output;
  // }

  // public function xpager_offset($offset = 0, $limit = 10) {
  //   $page = \Drupal::request()->query->get('page');
  //   if ($page && is_numeric($page) && $page > 0) {
  //     return ($page * $offset) - ($page * $limit);
  //   }
  //   return 0;
  // }

  // public function xget_promo_articles_for_term($entity, $count = 4) {
  //   $nids_to_include = [];
  //   if ($promoted_nids = $entity->get('field_promo_articles')->getValue()) {
  //     foreach ($promoted_nids as $promoted_nid) {
  //       $nids_to_include[] = $promoted_nid['target_id'];
  //     }

  //     if (count($nids_to_include) < $count) {
  //       $limit = $count - count($nids_to_include);
  //       $more_nids = xget_latest_nids_from_tid($entity->id(), $limit, $nids_to_include);
  //       foreach ($more_nids as $nid) {
  //         $nids_to_include[] = $nid;
  //       }
  //     }
  //     $nids_to_include = implode('+', $nids_to_include);
  //   }
  //   return $nids_to_include;
  // }

  // public function xget_author_initials($full_name = '') {
  //   $output = '';
  //   $full_name_array = explode(' ', $full_name);
  //   if (count($full_name_array) > 1) {
  //     $first_name_letter = mb_substr($full_name_array[0], 0, 1);
  //     $last_name_letter = mb_substr($full_name_array[1], 0, 1);
  //     $output = $first_name_letter . $last_name_letter;
  //   }
  //   else if (count($full_name_array) == 1) {
  //     $output = mb_substr($full_name_array[0], 0, 1);
  //   }
  //   return $output;
  // }

  // public function xget_file_size($fid = '') {
  //   $file_size = '';
  //   if ($fid) {
  //     $file = \Drupal\file\Entity\File::load($fid);
  //     if ($file) {
  //       $file_array = explode('.', $file->getFilename());
  //       // dd($file_array[count($file_array) - 1);
  //       $file_type = xstrtoupper($file_array[count($file_array ) - 1]);

  //       $file_size = format_size($file->getSize()) . ' - ' . $file_type;
  //     }
  //   }
  //   return $file_size;
  // }

}
