<?php

namespace Drupal\pixual;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;

/**
 * Provides a MyModuleSubscriber.
 */
class PixualBoot implements EventSubscriberInterface {

  /**
   * // only if KernelEvents::REQUEST !!!
   * @see Symfony\Component\HttpKernel\KernelEvents for details
   *
   * @param Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   */
  public function PixualBootLoad(GetResponseEvent $event) {
    global $admin_url_identifier, $pixual;
    $route_name = \Drupal::routeMatch()->getRouteName();
    if (in_array($route_name, [
      'user.register',
      'user.pass',
      'user.pass.http',
      'user.page',
      'user.edit',
      'user.login',
      'user.login.http',
      'user.login_status.http',
      'user.reset.login',
      'user.reset',
      'user.reset.form',
    ])) {
      \Drupal::service('page_cache_kill_switch')->trigger();
      if ($admin_url_identifier && !pixual_is_admin_url()) {
        user_logout();
        $redirect = new RedirectResponse(Url::fromRoute('<front>')->toString());
        $redirect->send();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['PixualBootLoad', 20];
    return $events;
  }
}

