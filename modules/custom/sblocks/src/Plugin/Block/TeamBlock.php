<?php

namespace Drupal\sblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'Team' Block.
 *
 * @Block(
 *   id = "sblocks_team",
 *   admin_label = @Translation("Team"),
 *   category = @Translation("sblock"),
 * )
 */
class TeamBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $team_terms = [];
    $team_terms = xget_vocabulary_tree_loaded('team');
    foreach ($team_terms as $k => $team) {
    $team_terms[$k] = $team->hasTranslation($language) ? $team->getTranslation($language) : $team;
    }
    $data = [
      'team_terms' => $team_terms,
    ];

    return [
      '#theme' => 'sblocks_team',
      '#data' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $node = \Drupal::routeMatch()->getParameter('node');

    $show = ($node && in_array($node->bundle(), ['page']));
    return AccessResult::allowedIf($show);
  }

}
