<?php

namespace Drupal\spages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\entityqueue\Entity\EntitySubqueue;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

class SPagesController extends ControllerBase {

  public function home() {
    // $nids = xget_queue_ids('homepage');
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $project = [];
    if (EntitySubqueue::load('homepage')->get('items')->referencedEntities()) {
      $project = EntitySubqueue::load('homepage')->get('items')->referencedEntities()[0]->getTranslation($language) ? EntitySubqueue::load('homepage')->get('items')->referencedEntities()[0]->getTranslation($language) : '';
    }
    $queue = EntitySubqueue::load('homepage')->hasTranslation($language) ? EntitySubqueue::load('homepage')->getTranslation($language) : EntitySubqueue::load('homepage');
    // $team_terms = [];
    // $team_terms = xget_vocabulary_tree_loaded('team');
    $team_terms = xget_queue_ids('team');
    // foreach ($team_terms as $k => $team) {
    //   $team_terms[$k] = $team->hasTranslation($language) ? $team->getTranslation($language) : $team;
    // }
    $schedule_ids = xget_queue_ids('schedule');
    $data = [
      'project' => $project,
      'team_terms' => implode('+', $team_terms),
      'schedule_ids' => implode('+', $schedule_ids),
      'queue' => $queue,
    ];

    return [
      '#theme' => 'home',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'imbalance/home',
        ],
      ],
    ];
  }

  public function contact() {
    return [
      '#theme' => 'contact',
      '#attached' => [
        'library' => [
          'imbalance/form',
        ],
      ],
    ];
  }

  public function gallery() {
    $gallery = EntitySubqueue::load('gallery')->field_gallery;
    $seo = EntitySubqueue::load('gallery')->field_seo_text->value;
    return [
      '#theme' => 'gallery',
      '#data' => [
        'gallery' => $gallery,
        'seo' => $seo,
      ],
      '#attached' => [
        'library' => [
          'imbalance/gallery',
        ],
      ],
    ];
  }
}
