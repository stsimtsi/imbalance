<?php

/**
 * @file
 * Theme functions for the sofia_οembed module.
 */

use Drupal\Core\Url;

// /**
//  * Prepares variables for media_entity_sofia_embed templates.
//  *
//  * Default template: media-sofia-embed.html.twig.
//  */
// function template_preprocess_media_sofia_embed(&$variables) {
//   // Build the embed url.
//   $uri = 'https://w.soundcloud.com/player/';

//   // Build the query.
//   $query = [
//     'url' => 'https://api.soundcloud.com/' . $variables['id'],
//   ];

//   // Add visual.
//   $query['visual'] = ($variables['type'] == 'visual');

//   // Add options.
//   foreach ($variables['options'] as $option => $value) {
//     $query[$option] = $value == '0' ? 'false' : 'true';
//   }

//   $url = Url::fromUri($uri, ['query' => $query]);
//   $variables['url'] = $url->toString();
// }
