<?php

namespace Drupal\Tests\media_entity_sofia_embed\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests for Soundcloud embed formatter.
 *
 * @group media_entity_sofia_embed
 */
class SofiaEmbedFormatterTest extends BrowserTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'media_entity_sofia_embed',
    'media',
    'link',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Setup standalone media urls from the settings.
    $this->config('media.settings')->set('standalone_url', true)->save();
    $this->refreshVariables();
    // Rebuild routes.
    \Drupal::service('router.builder')->rebuild();

    // Create an admin user with permissions to administer and create media.
    $account = $this->drupalCreateUser([
      // Media entity permissions.
      'view media',
      'create media',
      'update media',
      'update any media',
      'delete media',
      'delete any media',
    ]);

    // Login the user.
    $this->drupalLogin($account);
  }

  /**
   * Tests adding and editing a soundcloud embed formatter.
   */
  public function testSofiaEmbedFormatter() {
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface */
    $entity_display_repository = \Drupal::service('entity_display.repository');

    $media_type = $this->createMediaType('sofia_embed', ['bundle' => 'sofia_embed']);

    $source_field = $media_type->getSource()->getSourceFieldDefinition($media_type);
    $this->assertSame('field_media_sofia_embed', $source_field->getName());
    $this->assertSame('string', $source_field->getType());

    // Set form and view displays.
    $entity_display_repository->getFormDisplay('media', $media_type->id(), 'default')
      ->setComponent('field_media_sofia_embed', [
        'type' => 'string_textfield',
      ])
      ->save();

    $entity_display_repository->getViewDisplay('media', $media_type->id(), 'full')
      ->setComponent('field_media_sofia_embed', [
        'type' => 'sofia_embed',
      ])
      ->save();

    // Create a soundcloud media entity.
    $this->drupalGet('media/add/' . $media_type->id());

    $page = $this->getSession()->getPage();
    $page->fillField('name[0][value]', 'SofiaEmbed');
    $page->fillField('field_media_sofia_embed[0][value]', 'https://soundcloud.com/winguy/billie-jean-remix-ft');
    $page->pressButton('Save');

    // Assert "has been created" text
    $this->assertText('has been created');

    // Get to the media page.
    $medias = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties([
      'name' => 'SofiaEmbed',
    ]);
    /** @var \Drupal\media\MediaInterface */
    $media = reset($medias);
    $this->drupalGet(Url::fromRoute('entity.media.canonical', ['media' => $media->id()])->toString());
    $this->assertSession()->statusCodeEquals(200);

    // Assert that the formatter exists on this page.
    $this->assertElementPresent('iframe[src*="soundcloud"]');
  }
}
