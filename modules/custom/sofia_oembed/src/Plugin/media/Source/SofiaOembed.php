<?php

namespace Drupal\sofia_oembed\Plugin\media\Source;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Sofia oembed entity media source.
 *
 * @MediaSource(
 *   id = "sofia_oembed",
 *   label = @Translation("Sofia oembed"),
 *   allowed_field_types = {"string", "string_long", "link"},
 *   default_thumbnail_filename = "generic2.png",
 *   description = @Translation("Provides business logic and metadata for embed."),
 *   forms = {
 *     "media_library_add" = "\Drupal\sofia_oembed\Form\SofiaOembedForm"
 *   }
 * )
 */
class SofiaOembed extends MediaSourceBase {

  /**
   * sofia oembed attributes.
   *
   * @var array
   */
  protected $sofia_oembed;

  /**
   * Config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Http Client Interface.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, ClientInterface $httpClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    $attributes = [
      'type' => $this->t('Resource type'),
      'title' => $this->t('Resource title'),
      'author_name' => $this->t('The name of the author/owner'),
      'author_url' => $this->t('The URL of the author/owner'),
      'provider_name' => $this->t("The name of the provider"),
      'provider_url' => $this->t('The URL of the provider'),
      'cache_age' => $this->t('Suggested cache lifetime'),
      'default_name' => $this->t('Default name of the media item'),
      'thumbnail_uri' => $this->t('Local URI of the thumbnail'),
      'thumbnail' => $this->t('Url of the thumbnail'),
      'thumbnail_width' => $this->t('Thumbnail width'),
      'thumbnail_height' => $this->t('Thumbnail height'),
      'url' => $this->t('The source URL of the resource'),
      'width' => $this->t('The width of the resource'),
      'height' => $this->t('The height of the resource'),
      'html' => $this->t('The HTML representation of the resource'),
    ];
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $content_url = $this->getMediaUrl($media);
    if ($content_url === FALSE) {
      return FALSE;
    }

    $data = $this->oEmbed($content_url);
    if ($data === FALSE) {
      return FALSE;
    }

    $file_system = \Drupal::service('file_system');

    switch ($attribute_name) {
      case 'default_name':
        $data_url = $data['url'] ? $data['url'] : $content_url;
        return $data['title'] ? $data['title'] : $data_url;

      case 'thumbnail_uri':
        if ($data['thumbnail_url'] && $data['provider_name'] != 'TikTok') {
          $destination = $this->configFactory->get('sofia_oembed.settings')->get('thumbnail_destination');
          $local_uri = $destination . '/' . md5($data['thumbnail_url']) . '-' . pathinfo($data['thumbnail_url'], PATHINFO_BASENAME);
          if (strpos($local_uri, '?') !== false) {
            $local_uri = explode('?', $local_uri)[0];
          }
          // Save the file if it does not exist.
          if (!file_exists($local_uri)) {
            $file_system->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

            $image = file_get_contents($data['thumbnail_url']);
            $file_system->saveData($image, $local_uri, FileSystemInterface::EXISTS_REPLACE);
          }

          return $local_uri;
        }
        return parent::getMetadata($media, $attribute_name);

      case 'thumbnail':
        return isset($data['thumbnail_url']) ? $data['thumbnail_url'] : '';

      case 'type':
        return isset($data['type']) ? $data['type'] : '';

      case 'title':
        return $data['title'];

      case 'author_name':
        return isset($data['author_name']) ? $data['author_name'] : '';

      case 'author_url':
        return isset($data['author_url']) ? $data['author_url'] : '';

      case 'provider_name':
        return isset($data['provider_name']) ? $data['provider_name'] : '';

      case 'provider_url':
        return isset($data['provider_url']) ? $data['provider_url'] : '';

      case 'thumbnail_width':
        return isset($data['thumbnail_width']) ? $data['thumbnail_width'] : '';

      case 'thumbnail_height':
        return isset($data['thumbnail_height']) ? $data['thumbnail_height'] : '';

      case 'url':
        return isset($data['url']) ? $data['url'] : '';

      case 'width':
        return isset($data['width']) ? $data['width'] : '';

      case 'height':
        return isset($data['height']) ? $data['height'] : '';

      case 'html':
        return isset($data['html']) ? $data['html'] : '';

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * Returns the track id from the source_url_field.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return string|bool
   *   The track if from the source_url_field if found. False otherwise.
   */
  protected function getMediaUrl(MediaInterface $media) {
    $source_field = $this->getSourceFieldDefinition($media->bundle->entity);
    $field_name = $source_field->getName();
    if ($media->hasField($field_name)) {
      $property_name = $source_field->getFieldStorageDefinition()->getMainPropertyName();
      return $media->{$field_name}->{$property_name};
    }
    return FALSE;
  }

    /**
   * Returns oembed data for a Sofia oembed url.
   *
   * @param string $url
   *   The Url.
   *
   * @return array
   *   An array of oembed data.
   */
  protected function oEmbed($url) {
    $this->sofia_oembed = &drupal_static(__FUNCTION__ . hash('md5', $url));
    global $xparam;
    $access_token = isset($xparam['access_token']) ? $xparam['access_token'] : '';

    if (!isset($this->sofia_oembed)) {
      $endpoint_url = '';
      $response = \Drupal::httpClient()->get('https://oembed.com/providers.json');
      $providers = json_decode((string) $response->getBody(), true);

      foreach ($providers as $item) {
        foreach($item['endpoints'] as $endpoint) {
          if (isset($endpoint['schemes']) && $endpoint['schemes']) {
            foreach($endpoint['schemes'] as $scheme) {
              $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
              if (preg_match("|^$regexp$|", $url)) {
                $endpoint_url = $item['endpoints'][0]['url'] . '?url=' . urlencode($url);
                $endpoint_url = str_replace('{format}', 'json', $endpoint_url);
                if (in_array($item['provider_name'], ['Facebook', 'Instagram'])) {
                  $endpoint_url = $endpoint_url . '&access_token=' . $access_token;
                }
                break 3;
              }
            }
          }
        }
      }
      
      try {
        $response = $this->httpClient->get($endpoint_url);
        $this->sofia_oembed = Json::decode((string) $response->getBody());
      }
      catch (ClientException $e) {
        $this->sofia_oembed = FALSE;
      }
    }

    return $this->sofia_oembed;
  }
}
