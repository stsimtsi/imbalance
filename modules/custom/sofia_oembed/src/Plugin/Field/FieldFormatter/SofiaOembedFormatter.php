<?php

namespace Drupal\sofia_oembed\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sofia_oembed\Plugin\media\Source\SofiaOembed;

/**
 * Plugin implementation of the 'sofia_oembed' formatter.
 *
 * @FieldFormatter(
 *   id = "sofiaoembed_embed",
 *   label = @Translation("Sofia oembed"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *     "link",
 *   }
 * )
 */
class SofiaOembedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '100%',
      'height' => '450',
      'view' => ''
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
      '#min' => 1,
      '#required' => TRUE,
      '#description' => $this->t('Width of embedded player.'),
    ];

    $elements['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
      '#min' => 1,
      '#required' => TRUE,
      '#description' => $this->t('Height (px) of embedded player.'),
    ];

    $elements['view'] = [
      '#type' => 'textfield',
      '#title' => $this->t('View'),
      '#default_value' => $this->getSetting('view'),
      '#description' => $this->t('View of embedded player.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [
      $this->t('Width: @width', [
        '@width' => $this->getSetting('width'),
      ]),
      $this->t('Height: @height', [
        '@height' => $this->getSetting('height'),
      ]),
      $this->t('View: @view', [
        '@view' => $this->getSetting('view'),
      ]),
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getEntity();

    $element = [];
    if (($source = $media->getSource()) && $source instanceof SofiaOembed) {
      /** @var \Drupal\media\MediaTypeInterface $item */
      foreach ($items as $delta => $item) {

        if ($url = $source->getMetadata($media, 'html')) {
          try {
            $response = \Drupal::httpClient()->get('https://oembed.com/providers.json');
            $providers = json_decode((string) $response->getBody(), true);
          }
          catch (ClientException $e) {
            $providers = FALSE;
          }

          foreach ($providers as $item){
            if(xstrtoupper($item['provider_name']) == xstrtoupper($source->getMetadata($media, 'provider_name'))) {
              $provider = $item;
            }
          }

          preg_match('/src="([^"]+)"/', $source->getMetadata($media, 'html'), $embed_url);
          $embed_url = str_replace('src=', '', $embed_url[0]);
          $embed_url = str_replace('"', '', $embed_url);
          $embed_url = explode("?", $embed_url)[0];
          $element[$delta] = [
            '#theme' => 'media_sofia_oembed',
            '#url' => $url,
            '#thumbnail' => $source->getMetadata($media, 'thumbnail'),
            '#type' => $source->getMetadata($media, 'type'),
            '#provider_name' => $source->getMetadata($media, 'provider_name'),
            '#embed_url' => $embed_url,
            '#iframe' => $source->getMetadata($media, 'html'),
            '#width' => $this->getSetting('width'),
            '#height' => $this->getSetting('height'),
            '#view' => $this->getSetting('view'),
          ];
        }
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getTargetEntityTypeId() === 'media') {
      return TRUE;
    }
    return FALSE;
  }

}
