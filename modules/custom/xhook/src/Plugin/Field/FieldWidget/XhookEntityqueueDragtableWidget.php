<?php

namespace Drupal\xhook\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

use Drupal\entityqueue\Plugin\Field\FieldWidget\EntityqueueDragtableWidget;

/**
 * Plugin implementation of the 'xhook_entityqueue_dragtable' widget.
 *
 * @FieldWidget(
 *   id = "xhook_entityqueue_dragtable",
 *   label = @Translation("Pixual Autocomplete (draggable table)"),
 *   description = @Translation("An autocomplete text field with a draggable table."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class XhookEntityqueueDragtableWidget extends EntityqueueDragtableWidget {

  /**
   * The unique HTML ID of the widget's wrapping element.
   *
   * @var string
   */
  protected $wrapperId;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    assert($items instanceof EntityReferenceFieldItemListInterface);
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');

    $referenced_entities = $items->referencedEntities();
    $field_name = $this->fieldDefinition->getName();

    if (isset($referenced_entities[$delta])) {
      $entity = $entity_repository->getTranslationFromContext($referenced_entities[$delta]);
      $entity_label = ($this->getSetting('link_to_entity') && !$entity->isNew()) ? $entity->toLink()->toString() : $entity->label();

      $element += [
        '#type' => 'container',
        '#attributes' => ['class' => ['form--inline']],
        '_number' => [
          '#type' => 'item',
          '#markup' => '<strong>' . ($delta + 1) . ':</strong>',
          '#weight' => -1,
        ],
        'target_id' => [
          '#type' => 'item',
          '#markup' => ($entity->access('view label')) ? $entity_label : t('- Restricted access -'),
          '#default_value' => !$referenced_entities[$delta]->isNew() ? $referenced_entities[$delta]->id() : NULL,
          '#weight' => 0,
        ],
        '_edit' => $referenced_entities[$delta]->toLink($this->t('Edit'), 'edit-form', ['query' => \Drupal::destination()->getAsArray()])->toRenderable() + [
          '#attributes' => ['class' => ['form-item']],
          '#access' => (bool) $this->getSetting('link_to_edit_form'),
        ],
        '_remove' => [
          '#type' => 'submit',
          '#name' => implode('_', array_merge($form['#parents'], [$field_name, $delta])) . '_remove',
          '#delta' => $delta,
          '#value' => $this->t('Remove'),
          '#attributes' => ['class' => ['remove-item-submit', 'align-right']],
          '#limit_validation_errors' => [array_merge($form['#parents'], [$field_name])],
          '#submit' => [[get_class($this), 'removeSubmit']],
          '#ajax' => [
            'callback' => [get_class($this), 'removeAjax'],
            'wrapper' => $this->getWrapperId(),
            'effect' => 'fade',
          ],
        ],
      ];
    }

    return $element;
  }
}
