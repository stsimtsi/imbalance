<?php

namespace Drupal\xmisc\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change the page callback for Taxonomy Term pages.
      if ($route = $collection->get('entity.taxonomy_term.canonical')) {
        $route->setDefault('_controller', '\Drupal\xmisc\Controller\XMiscController::taxonomyPage');
      }
  }

}
