<?php

namespace Drupal\xmisc\Controller;

use Drupal\Core\Controller\ControllerBase;

class XMiscController extends ControllerBase {

  public function taxonomyPage() {
    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $page = \Drupal::request()->query->get('page');

    $output = [
      '#data' => [
        'term' => $term,
        'page' => $page,
      ],
    ];

    switch ($term->bundle()) {
      case 'author':
        $output['#theme'] = 'xmisc_author';
        $output['#attached'] = [
          'library' => [
//            'THEMENAME/author',
          ],
        ];
        break;
      case 'tags':
        $output['#theme'] = 'xmisc_category';
        $output['#tag'] = true;
      break;
      case 'category':
        if ($term->parent->target_id != 0) {
          $output['#theme'] = 'xmisc_category';
          $output['#child'] = true;

        }
        else {
          $output['#theme'] = 'xmisc_category';
        }
        break;
      case 'region':
        $region_terms = xget_vocabulary_tree_loaded('region', 0, 1);
        $depth = count(\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($term->id()));
        // if ($depth == 1) {
          $output['#theme'] = 'xmisc_region';
          if ($term->field_governor_name->value) {
            $output['#term_alias'] = \Drupal::service('path_alias.manager')->getAliasByPath('/governor/' . $term->id());
          }
          $output['#region_terms'] = $region_terms;
          $output['#depth'] = $depth;
        // }
        break;
      default:
        $output['#theme'] = 'xmisc_list';
    }

    return $output;
  }
}
