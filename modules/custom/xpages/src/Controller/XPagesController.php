<?php

namespace Drupal\xpages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\entityqueue\Entity\EntitySubqueue;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

class XPagesController extends ControllerBase {

  public function home() {
    // $nids = xget_queue_ids('homepage');
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $project = [];
    if (EntitySubqueue::load('homepage')->get('items')->referencedEntities()) {
      $project = EntitySubqueue::load('homepage')->get('items')->referencedEntities()[0]->getTranslation($language) ? EntitySubqueue::load('homepage')->get('items')->referencedEntities()[0]->getTranslation($language) : '';
    }
    $queue = EntitySubqueue::load('homepage')->hasTranslation($language) ? EntitySubqueue::load('homepage')->getTranslation($language) : EntitySubqueue::load('homepage');
    $team_terms = [];
    $team_terms = xget_vocabulary_tree_loaded('team');
    foreach ($team_terms as $k => $team) {
      $team_terms[$k] = $team->hasTranslation($language) ? $team->getTranslation($language) : $team;
    }
    $data = [
      'project' => $project,
      'team_terms' => $team_terms,
      'queue' => $queue,
    ];

    return [
      '#theme' => 'home',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'imbalance/home',
        ],
      ],
    ];
  }

  public function contact() {
    return [
      '#theme' => 'contact',
      '#attached' => [
        'library' => [
          'imbalance/form',
        ],
      ],
    ];
  }

  public function gallery() {
    $gallery = EntitySubqueue::load('gallery')->field_gallery;
    return [
      '#theme' => 'gallery',
      '#data' => [
        'gallery' => $gallery,
      ],
      '#attached' => [
        'library' => [
          'imbalance/gallery',
        ],
      ],
    ];
  }
}
